<x-invitado-layout>
    <div class="container mx-auto h-full flex flex-1 justify-center items-center">
        <div class="w-full max-w-screen-lg">
            <div class="leading-loose m-4 px-10 pt-5 pb-10 bg-white rounded shadow-xl">
                <div class="text-right">
                    <a href="{{ route('dashboard') }}" class="text-sm text-gray-700 underline">Acceso</a>
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Registro</a>
                </div>
                <div>
                    <div class="bg-indigo-400 rounded p-7 text-white mb-3">
                        <h2 class="font-bold text-lg mb-2">¡Bienvenido a Gestionam.ehhh.es!</h2>
                        <p>Somos una aplicación para organizar tu economía. Ofrecemos una forma divertiva de llevar tu economía personal,
                            controlando tus gastos e ingresos y tratando de adivinar los gastos e ingresos futuros. </p>
                        <p>Pero esto ya lo puedes encontrar en cualquier aplicación bancaria o cientos de aplicaciones gratuitas. ¿Por qué
                            utilizar nuestra aplicación? A continuación te mostramos nuestras principales ventajas. Esperamos convencerte.</p>
                    </div>
                    <div class="p-1 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-3">
                        <div class="max-w-sm rounded overflow-hidden shadow-lg">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">Multi-cuenta</div>
                                <p class="text-center text-indigo-400 text-5xl">
                                    <i class="fas fa-piggy-bank" aria-hidden="true"></i>
                                </p>
                                <p>Mantén los datos de todas las cuentas que tengas. Contabiliza el dinero que tienes en tu monedero o en
                                la hucha.</p>
                            </div>
                        </div>
                        <div class="max-w-sm rounded overflow-hidden shadow-lg">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">Multi-origen</div>
                                <p class="text-center text-indigo-400 text-5xl"><i class="fas fa-store-alt" aria-hidden="true"></i></p>
                                <p>Puedes mantener los datos de todas las fuentes de ingresos y gastos, y consultar todo el dinero que llevas
                                gastado o ganado de cada uno de ellos.</p>
                            </div>
                        </div>
                        <div class="max-w-sm rounded overflow-hidden shadow-lg">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">Multi-moneda</div>
                                <p class="text-center text-indigo-400 text-5xl"><i class="fab fa-bitcoin" aria-hidden="true"></i></p>
                                <p>Cada cuenta o cartera puede estar asociado a una moneda. Podrás mantener tu cuenta del banco en euros y tu
                                wallet en bitcoins.</p>
                            </div>
                        </div>
                        <div class="max-w-sm rounded overflow-hidden shadow-lg">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">Predictivo</div>
                                <p class="text-center text-indigo-400 text-5xl"><i class="fas fa-meh-rolling-eyes" aria-hidden="true"></i></p>
                                <p>Adivina los gastos e ingresos que están por venir y calcula cuando podrás comprar ese capricho que tienes en mente.</p>
                            </div>
                        </div>
                        <div class="max-w-sm rounded overflow-hidden shadow-lg">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">Te escuchamos</div>
                                <p class="text-center text-indigo-400 text-5xl"><i class="fas fa-comment-dots" aria-hidden="true"></i></p>
                                <p>Queremos que la aplicación sea útil para todo el mundo. Si tienes alguna idea, no dudes en <a href="mail_to:leroj7@gmail.com">escribirnos</a>.</p>                            </div>
                        </div>
                        <div class="max-w-sm rounded overflow-hidden shadow-lg">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">Anónimo</div>
                                <p class="text-center text-indigo-400 text-5xl"><i class="fas fa-user-secret" aria-hidden="true"></i></p>
                                <p>No queremos ningún dato personal tuyo, sólo un nombre de usuario.</p>
                            </div>
                        </div>
                        <div class="max-w-sm rounded overflow-hidden shadow-lg">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">Gratuito</div>
                                <p class="text-center text-indigo-400 text-5xl"><i class="fab fa-creative-commons-zero" aria-hidden="true"></i></p>
                                <p>No te cobraremos por la aplicación, ni ahora ni NUNCA. Como mucho aspiramos a conseguir meter algo de publicidad para recuperar el esfuerzo empleado.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-invitado-layout>
