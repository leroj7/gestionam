<div x-show="confirmacion" id="centeredModal" class="modal-wrapper modal-is-open">
    <div @click="confirmacion=false" class="overlay close-modal"></div>
    <div class="modal modal-centered">
        <div class="modal-content shadow-lg p-5">
            <div class="border-b p-2 pb-3 pt-0 mb-4">
            <div class="flex justify-between items-center">
                Confirmación
                <span @click="confirmacion=false" class="close-modal cursor-pointer px-3 py-1 rounded-full bg-gray-100 hover:bg-gray-200">
                    <i class="fas fa-times text-gray-700"></i>
                </span>
            </div>
            </div>
            <!-- Modal content -->
            <p>La eliminación será irreversible</p>
            <p class="text-center">¿ Desea continuar ?</p>
            <p class="text-right mt-3">
                <button @click="confirmacion=false" class="bg-green-500 hover:bg-green-800 text-white font-bold py-3 px-4 rounded">
                    Cancelar
                </button>
                &nbsp;
                <button @click="document.getElementById('d'+actual).submit()" class="bg-red-500 hover:bg-red-800 text-white font-bold py-3 px-4 rounded">
                    Eliminar
                </button>                    
            </p>
        </div>
    </div>
</div>       
