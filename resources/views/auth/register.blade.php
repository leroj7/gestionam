<x-invitadocompleto-layout>
    <div class="container mx-auto h-full flex flex-1 justify-center items-center">
        <div class="w-full max-w-lg">
            <div class="leading-loose">
                <form method="POST" action="{{ route('register') }}" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
                    @csrf
                    <div class="-mt-4"><img src="/img/logo.svg" title="Gestionam-ehhh.es" class="w-1/12" /></div>
                    <p class="text-gray-800 font-medium text-center text-lg font-bold">Registro</p>
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <div>
                        <label class="block text-sm text-gray-00" for="name">Nombre</label>
                        <x-input id="name" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" type="text" name="name" :value="old('name')" placeholder="Nombre completo" aria-label="Nombre completo" required autofocus />
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-00" for="email">Correo electrónico</label>
                        <x-input id="email" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" 
                            placeholder="Correo electrónico" aria-label="email" type="email" name="email" :value="old('email')" required />
                    </div>                    
                    <div class="mt-2">
                        <label class="block text-sm text-gray-00" for="password">Clave</label>
                        <x-input id="password" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" 
                            placeholder="******" aria-label="clave" type="password" name="password" required autocomplete="new-password" />
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-00" for="password_confirmation">Confirmación de clave</label>
                        <x-input id="password_confirmation"  class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" 
                            placeholder="******" aria-label="clave" type="password" name="password_confirmation" required />
                    </div>
                    <div class="mt-4 items-center justify-between">
                        <x-button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded">
                            Registrar
                        </x-button>
                        <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 ml-1" href="{{ route('login') }}">
                            ¿Ya estás registrado?
                        </a>                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-invitadocompleto-layout>
