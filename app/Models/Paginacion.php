<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paginacion extends Model
{
    public $total, $pagina, $porPagina, $paginas, $limiteInferior, $limiteSuperior, $url, $paginaActualInf, $paginaActualSup;

    function __construct(int $total, int $pagina = 1, int $porPagina = 20) {
        parent::__construct();
        $this->total = $total;
        $this->pagina = $pagina;
        $this->porPagina = $porPagina;
        $this->paginas = $total/$porPagina > intval($total/$porPagina) ? intval($total/$porPagina)+1 : intval($total/$porPagina);
        $this->limiteInferior=$pagina-$porPagina>0 ? $pagina-$porPagina : 1;
        $this->limiteSuperior=$this->limiteInferior+($porPagina*2) < $this->paginas ? $this->limiteInferior+($porPagina*2) : $this->paginas;
        $this->paginaActualInf = (($pagina - 1) * $porPagina) + 1;
        $this->paginaActualSup = ($this->paginaActualInf + $porPagina) -1 > $total ? $total : ($this->paginaActualInf + $porPagina) - 1;
        $this->url=preg_replace('/(&)?pagina=[0-9]+/', "", $_SERVER["REQUEST_URI"]).(strpos($_SERVER["REQUEST_URI"], "?")===false ? "?consulta=1" : "");
    }

}
