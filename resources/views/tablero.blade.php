<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                ¡ Hola ! Según nuestros datos dispones de {{$total}} &euro;.
            </h2>
            <ion-content class="bg-white overflow-hidden shadow-xl sm:rounded-lg slider-carteras">
                <ion-slides pager="true" slidesPerView=3 freeMode="true" class="mt-2">
                    @foreach ($carteras as $cartera)
                        <ion-slide>
                            <div class="cartera shadow-lg bg-{{$colores[$loop->index%7]}}-400 border-l-8 hover:bg-$colores[$loop->index%7]-600 border-$colores[$loop->index%7]-600 mb-2 p-2 w-full mx-2">
                                <div class="p-4 flex flex-col">
                                    <a href="#" class="no-underline text-white text-2xl">
                                        {{$cartera['cantidad']}} &euro;
                                    </a>
                                    <a href="#" class="no-underline text-white text-lg">
                                        {{$cartera['nombre']}}
                                    </a>
                                </div>
                            </div>
                        </ion-slide>
                    @endforeach
                </ion-slides>
            </ion-content>
            <!-- Calendario -->
            <div class="container mx-auto py-2 md:py-24">
                <div class="bg-white rounded-lg shadow-xl overflow-hidden">
                    <div class="flex items-center justify-between py-2 px-6">
                        <div>
                            <span class="text-lg font-bold text-gray-800">{{$calendario->nombreMes()}}</span>
                            <span x-text="ano" class="ml-1 text-lg text-gray-600 font-normal">{{$calendario->ano}}</span>
                        </div>
                        <div class="border rounded-lg px-1" style="padding-top: 2px;">
                            <a href="/tablero?m={{$calendario->mesAnterior()}}&a={{$calendario->anoAnterior()}}">
                                <button 
                                    type="button"
                                    class="leading-none rounded-lg transition ease-in-out duration-100 inline-flex cursor-pointer hover:bg-gray-200 p-1 items-center" 
                                >
                                    <svg class="h-6 w-6 text-gray-500 inline-flex leading-none"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"/>
                                    </svg>  
                                </button>
                            </a>
                            <div class="border-r inline-flex h-6"></div>		
                            <a href="/tablero?m={{$calendario->mesSiguiente()}}&a={{$calendario->anoSiguiente()}}">
                                <button 
                                    type="button"
                                    class="leading-none rounded-lg transition ease-in-out duration-100 inline-flex items-center cursor-pointer hover:bg-gray-200 p-1" 
                                >
                                    <svg class="h-6 w-6 text-gray-500 inline-flex leading-none"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"/>
                                    </svg>									  
                                </button>
                            </a>
                        </div>
                    </div>	
                    <div class="-mx-1 -mb-1">
                        <div class="flex flex-wrap border-t border-l">
                            <div style="width: 14.28%;" class="text-center border-r border-b px-4 pt-2 text-white bg-indigo-400">L</div>
                            <div style="width: 14.28%;" class="text-center border-r border-b px-4 pt-2 text-white bg-indigo-400">M</div>
                            <div style="width: 14.28%;" class="text-center border-r border-b px-4 pt-2 text-white bg-indigo-400">X</div>
                            <div style="width: 14.28%;" class="text-center border-r border-b px-4 pt-2 text-white bg-indigo-400">J</div>
                            <div style="width: 14.28%;" class="text-center border-r border-b px-4 pt-2 text-white bg-indigo-400">V</div>
                            <div style="width: 14.28%;" class="text-center border-r border-b px-4 pt-2 text-white bg-indigo-400">S</div>
                            <div style="width: 14.28%;" class="text-center border-r border-b px-4 pt-2 text-white bg-indigo-400">D</div>
                            @for ($i = 0; $i < $calendario->huecos(); $i++)
                                <div 
                                    style="width: 14.28%; height: 120px"
                                    class="text-center border-r border-b px-4 pt-2"	
                                ></div>
                            @endfor
                            @foreach ($calendario->dias() as $dia)
                                <div style="width: 14.28%; height: 120px" class="px-4 pt-2 border-r border-b relative">
                                    <div>
                                        <a 
                                            href="{{ route( 'dia', ['m' => $calendario->mes, 'd' => $dia['dia'], 'a' => $calendario->ano]) }}"
                                            class="inline-flex w-6 h-6 items-center justify-center cursor-pointer text-center leading-none rounded-full transition ease-in-out duration-100 text-gray-700 hover:bg-blue-200"	
                                        >
                                            {{$dia['dia']}}
                                        </a>
                                    </div>
                                    <div style="height: 80px;" class="overflow-y-auto mt-1">
                                        @foreach ($dia['movimientos'] as $movimiento)
                                            @if ($movimiento->tipo == "I")
                                                <div title="{{$movimiento->descripcion}}"
                                                    class="px-2 py-1 rounded-lg mt-1 overflow-hidden border border-green-200 text-green-800 bg-green-100">
                                                    <p class="text-sm truncate leading-tight">
                                                        {{$movimiento->descripcion}}
                                                    </p>
                                                </div>
                                            @else
                                                @if ($movimiento->tipo == "A")
                                                    <div title="{{$movimiento->descripcion}}"
                                                        class="px-2 py-1 rounded-lg mt-1 overflow-hidden border border-indigo-200 text-indigo-800 bg-indigo-100">
                                                        <p class="text-sm truncate leading-tight">
                                                            {{$movimiento->descripcion}}
                                                        </p>
                                                    </div>
                                                @else
                                                    <div title="{{$movimiento->descripcion}}"
                                                        class="px-2 py-1 rounded-lg mt-1 overflow-hidden border border-red-200 text-red-800 bg-red-100">
                                                        <p class="text-sm truncate leading-tight">
                                                            {{$movimiento->descripcion}}
                                                        </p>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var slides = document.querySelector('ion-slides');
        slides.options = { slidesPerView: 3 };
    </script>
</x-app-layout>
