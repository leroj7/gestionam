<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Movimiento;

class DiasController extends Controller {
    public function index(Request $request) {
        $dia = $request->d;
        $mes = $request->m;
        $ano = $request->a;
        $fecha = $ano."-".$mes."-".$dia;
        $movimientos = Movimiento::whereBetween('fecha', [$fecha." 00:00:00", $fecha." 23:59:59"])
            ->get();
        return view('dias/index', [
            'movimientos' => $movimientos,
            'dia' => $dia,
            'mes' => $mes,
            'ano' => $ano,
            'ts' => \DateTime::createFromFormat('Y-m-d H:i:s', $fecha.' 00:00:00')->getTimestamp()
        ]);
    }
}
