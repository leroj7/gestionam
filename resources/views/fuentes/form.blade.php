<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                @if ($fuente->id != "")
                    {{$fuente->nombre}}
                @else
                    Nueva fuente de ingresos
                @endif
            </h2>
            <div class="leading-loose">
                <form action="{{ route('guardarfuente') }}" method="post" class="p-10 bg-white rounded shadow-xl">
                    @csrf
                    @if ($errors->any())
                        <div class="bg-red-300 mb-2 border border-red-300 px-4 py-3 rounded relative">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="text-red-700">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <div class="">
                        <label class="block text-sm text-gray-600" for="nombre">Nombre <span class="text-sm">(*)</span></label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="nombre" 
                            name="nombre" type="text" placeholder="Nombre de la fuente de ingresos"
                            required="true" aria-label="Nombre" value="{{$fuente->nombre}}">
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="Url">Url</label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="url" name="url" 
                            type="text" placeholder="Página web relacionada con la fuente" 
                            aria-label="Url" value="{{$fuente->url}}">
                    </div>
                    <div class="mt-6 text-right">
                        <input id="id" name="id" type="hidden" aria-label="Id" value="{{$fuente->id}}">
                        <button class="px-4 py-3 text-white font-light tracking-wider bg-green-500 hover:bg-green-800 rounded" type="submit">Guardar</button>
                    </div>
                    <p class="text-right text-sm">(*): Campo requerido</p>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
