<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Movimiento;

class MovimientosController extends Controller {
    public function eliminar($id) {
        $movimiento = Movimiento::find($id);
        if(empty($movimiento)) {
            return redirect( route('tablero') )->with('error', 1);
        }
        $movimiento->delete();
        return redirect( route('dia', ['m' => $movimiento->mes, 'd' => $movimiento->dia, 'a' => $movimiento->ano]) )->with('exito', 1);
    }

    public function editar($id) {
        $movimiento = Movimiento::find($id);
        if(empty($movimiento)) {
            return redirect( route('tablero') )->with('error', 1);
        }
        $fuentes = Auth::user()->fuentes->sortBy('nombre');
        $carteras = Auth::user()->carteras->sortBy('nombre');
        $dt = new \DateTime($movimiento->fecha);
        $ts = $dt->getTimestamp();
        $fecha = $dt->format('d/m/Y');
        return view('movimientos/form', [
            'movimiento' => $movimiento,
            'fuentes' => $fuentes,
            'carteras' => $carteras,
            'fecha' => $fecha, 
            'ts' => $ts
        ]);
    }

    public function guardar(Request $request) {
        $datos = $request->validate([
            'tipo'=>'required|max:1',
            'descripcion'=>'required|max:2000',
            'hora'=>'nullable',
            'fuente_id'=>'numeric|nullable',
            'cartera_id'=>'required|numeric',
            'cantidad'=>'required|numeric',
            'ts' =>'nullable',
            'id'=>'nullable'
        ]);
        if($datos["id"] != ""){
            $movimiento = Movimiento::find($datos["id"]);
        } else {
            $movimiento = new Movimiento();
        }
        $movimiento->asignar($datos);
        $movimiento->save();
        return $movimiento->tipo == 'A' 
            ? redirect( route('movimientoscartera', $movimiento->cartera_id) )->with('exito', 1)
            : redirect( route('dia', ['m' => $movimiento->mes, 'd' => $movimiento->dia, 'a' => $movimiento->ano]) )->with('exito', 1);
    }

    // public function index() {
    //     $orden = isset($_GET["orden"]) ? "balance" : "nombre";
    //     $fuentes = Auth::user()->fuentesConBalance()->get();
    //     if($orden == "balance") {
    //         $fuentes = $fuentes->sortByDesc($orden);
    //     } else {
    //         $fuentes = $fuentes->sortBy(function ($d, $k) use ($orden) {
    //             return strtolower($d[$orden]);
    //         });
    //     }
    //     return view('fuentes/index', [
    //         'orden' => $orden,
    //         'fuentes' => $fuentes
    //     ]);
    // }

    // public function movimientos(Request $request, $id) {
    //     $pagina = $request->pagina ?? 1;
    //     $fuente = Fuente::find($id);
    //     $movimientos = $fuente->movimientos()->orderBy('fecha', 'desc')
    //         ->limit(20)->offset(($pagina-1)*20)->get();
    //     $total = $fuente->movimientos()->count();
    //     $paginacion = new Paginacion($total, $pagina);
    //     return view('fuentes/movimientos', [
    //         'fuente' => $fuente,
    //         'movimientos' => $movimientos,
    //         'paginacion' => $paginacion
    //     ]);
    // }

    public function nuevo(Request $request) {
        $ts = $request->ts;
        $fecha = date('d-m-Y', $ts);
        $movimiento = new Movimiento();
        $fuentes = Auth::user()->fuentes->sortBy('nombre');
        $carteras = Auth::user()->carteras->sortBy('nombre');
        return view('movimientos/form', [
            'movimiento' => $movimiento,
            'fuentes' => $fuentes,
            'carteras' => $carteras,
            'fecha' => $fecha, 
            'ts' => $ts
        ]);
    }

}
