<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                Carteras
            </h2>
            <p class="text-right">
                <a href="{{ route('nuevacartera') }}" title="Nueva Cartera">
                    <button class="bg-green-500 hover:bg-green-800 text-white font-bold py-3 px-4 rounded">
                        Nueva cartera
                    </button>
                </a>
            </p>
            <p class="text-right text-sm mt-3">
                Orden: 
                @if ($orden != "balance")
                    Alfabetico
                @else
                    <a href="/carteras">Alfabético</a>
                @endif, 
                @if ($orden == "balance")
                    Balance
                @else
                    <a href="/carteras?orden=ingresos">Balance</a>
                @endif
            </p>
            <div class="bg-white rounded-lg shadow-xl overflow-auto mt-2">
                <table class="min-w-full bg-white">
                    <thead class="bg-indigo-400 text-white">
                        <tr>
                            <th class="w-1/4 text-left py-3 px-4 uppercase font-semibold text-sm">Cartera</th>
                            <th class="w-1/4 text-left py-3 px-4 uppercase font-semibold text-sm">Cantidad</th>
                            <th class="w-1/4 text-left py-3 px-4 uppercase font-semibold text-sm">Cantidad en €</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm"></th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-700">
                        @foreach ($carteras as $i => $cartera)
                            <tr >
                                <td class="w-1/4 text-left py-3 px-4">{{ $cartera->nombre }}</td>
                                <td class="w-1/4 text-left py-3 px-4">{{ floatval($cartera->cantidad_moneda) }} {{ str_replace("Euro", "€", $cartera->moneda) }}</td>
                                <td class="w-1/4 text-left py-3 px-4">{{ floatval($cartera->cantidad_euros) }} &euro;</td>
                                <td class="text-left py-3 px-4">
                                    <a href="{{ route( 'actualizarcantidadcartera', [$cartera->id]) }}" class="bg-green-500 hover:bg-green-800 cursor-pointer rounded p-1 mx-1 text-white tab-button">
                                        <i class="fas fa-money-bill-wave"></i>
                                    </a>
                                    <a href="{{ route( 'movimientoscartera', [$cartera->id]) }}" class="bg-green-500 hover:bg-green-800 cursor-pointer rounded p-1 mx-1 text-white tab-button">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="{{ route( 'editarcartera', [$cartera->id]) }}" class="bg-green-500 hover:bg-green-800 cursor-pointer rounded p-1 mx-1 text-white tab-button">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    @include('recursos.botonBorrar', ['objeto' => $cartera, 'ruta' => route('eliminarcartera', [$cartera->id])])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('recursos.confirmarEliminacion', [])
</x-app-layout>
