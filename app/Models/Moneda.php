<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class Moneda extends Model
{
    use HasFactory;

    public static function cotizacion($moneda){
        $cotizacion = Cache::remember($moneda, 3600, function () use ($moneda) {
            $client = new Client(); //GuzzleHttp\Client
            $res = $client->get("https://min-api.cryptocompare.com/data/price?fsym=".$moneda."&tsyms=USD,EUR");
            if($res->getStatusCode() == '200'){
                $valores = json_decode($res->getBody()->getContents());
                return ["dolares" => $valores->USD, "euros" => $valores->EUR];
            }
            return;
        });
        return $cotizacion;
    } 
    
    public function enEuros($cantidad) {
        if($this->nombre == 'Euro'){
            $res = $cantidad;
        } else {
            $cotizacion = $this->cotizacion($this->abreviatura);
            $res = round( $cotizacion['euros'] * $cantidad, 2);
        }
        return $res;
    }

}
