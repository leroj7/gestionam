<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                Fuentes de ingresos
            </h2>
            <p class="text-right">
                <a href="{{ route('nuevafuente') }}" title="Nueva fuente">
                    <button class="bg-green-500 hover:bg-green-800 text-white font-bold py-3 px-4 rounded">
                        Nueva fuente
                    </button>
                </a>
            </p>
            <p class="text-right text-sm mt-3">
                Orden: 
                @if ($orden != "balance")
                    Alfabetico
                @else
                    <a href="/fuentes-de-ingresos">Alfabético</a>
                @endif, 
                @if ($orden == "balance")
                    Ingresos
                @else
                    <a href="/fuentes-de-ingresos?orden=ingresos">Ingresos</a>
                @endif
            </p>
            <div class="bg-white rounded-lg shadow-xl overflow-auto mt-2">
                <table class="min-w-full bg-white">
                    <thead class="bg-indigo-400 text-white">
                        <tr>
                            <th class="w-1/3 text-left py-3 px-4 uppercase font-semibold text-sm">Fuente</th>
                            <th class="w-1/3 text-left py-3 px-4 uppercase font-semibold text-sm">Balance</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm"></th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-700">
                        @foreach ($fuentes as $i => $fuente)
                            <tr >
                                <td class="w-1/3 text-left py-3 px-4">{{ $fuente->nombre }}</td>
                                <td class="w-1/3 text-left py-3 px-4">{{ $fuente->balance }}</td>
                                <td class="text-left py-3 px-4">
                                    <a href="{{ route( 'movimientosfuente', [$fuente->id]) }}" class="bg-green-500 hover:bg-green-800 cursor-pointer rounded p-1 mx-1 text-white tab-button">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="{{ route( 'editarfuente', [$fuente->id]) }}" class="bg-green-500 hover:bg-green-800 cursor-pointer rounded p-1 mx-1 text-white tab-button">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    @include('recursos.botonBorrar', ['objeto' => $fuente, 'ruta' => route('eliminarfuente', [$fuente->id])])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('recursos.confirmarEliminacion', [])
</x-app-layout>
