<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function carteras()
    {
        return $this->hasMany(Cartera::class);
    }

    public function fuentes() {
        return $this->hasMany(Fuente::class);
    }

    public function fuentesConBalance() {
        return Fuente::select(
                "id", "nombre",
                DB::raw("(SELECT IFNULL(sum(cantidad_euros), 0) FROM movimientos WHERE tipo='I' AND fuente_id=fuentes.id)".
                    "-(SELECT IFNULL(sum(cantidad_euros), 0) FROM movimientos WHERE tipo='G' AND fuente_id=fuentes.id) as 'balance'")
            )
            ->where('user_id', $this->id);
    }

}
