<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Calendario;

class TableroController extends Controller {
    public function index() {
        $carteras = Auth::user()->carteras()->get();
        $total = 0;
        $cantidades = [];
        foreach($carteras as $f){
            $c = $f->cantidad;
            $total += $c['euros'];
            $cantidades[] = ["cantidad" => $c['euros'], "nombre" => $f->nombre];
        }
        usort($cantidades, function($a, $b) {
            if($a['cantidad'] == $b['cantidad']) {
                return 0;
            }
            return $a['cantidad'] < $b['cantidad'] ? 1 : -1;
        });
        $a = isset($_GET['a']) ? $_GET['a'] : date('Y');
        $m = isset($_GET['m']) ? $_GET['m'] : date('m');
        $calendario = Calendario::obtener($m, $a);
        return view('tablero', [
            'calendario' => $calendario,
            'total' => $total,
            'carteras' => $cantidades,
            'colores' => ['red', 'yellow', 'green', 'blue', 'indigo', 'purple', 'pink']
        ]);
    }
}
