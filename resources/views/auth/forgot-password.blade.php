<x-invitadocompleto-layout>
    <div class="container mx-auto h-full flex flex-1 justify-center items-center">
        <div class="w-full max-w-lg">
            <div class="leading-loose">
                <form method="POST" action="{{ route('password.email') }}" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
                    @csrf
                    <div class="-mt-4"><img src="/img/logo.svg" title="Gestionam-ehhh.es" class="w-1/12" /></div>
                    <p class="text-gray-800 font-medium text-center text-lg font-bold">Olvido de contraseña</p>
                    <div class="mb-4 text-sm text-gray-600">
                        ¿Olvidaste tu contraseña? No hay problema. Indícanos tu correo electrónico y te enviaremso un enlace desde 
                        el que podrás introducir una nueva.
                    </div>                    
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <x-auth-session-status class="mb-4 font-medium text-sm text-green-600" :status="session('status')" />
                    <div>
                        <label class="block text-sm text-gray-00" for="email">Correo electrónico</label>
                        <x-input id="email" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" 
                            placeholder="Correo electrónico" aria-label="email" type="email" name="email" :value="old('email')" required autofocus />
                    </div>
                    <div class="flex items-center justify-end mt-4">
                        <x-button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded">
                            Recibir enlace
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-invitadocompleto-layout>



