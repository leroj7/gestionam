<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cartera extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function asignar($datos){
        $this->nombre = $datos['nombre'];
        $this->descripcion = $datos['descripcion'];
        $this->moneda = $datos['moneda'];
        $this->user_id = Auth::user()->id;
    }

    public function getCantidadAttribute(){
        $cantidad = $this->movimientos()->where("tipo", "<>", 'G')->selectRaw('SUM(cantidad) as ingresos')->first()->ingresos - 
            $this->movimientos()->where("tipo", "=", 'G')->selectRaw('SUM(cantidad) as gastos')->first()->gastos;
        return [
            'moneda' => $cantidad,
            'euros' => round($this->moneda == "Euro" ? $cantidad : ($cantidad * Moneda::cotizacion($this->moneda)["euros"]), 2)
        ];
    }  
    
    public function movimientos() {
        return $this->hasMany(Movimiento::class);
    }

    public function getObjetoMonedaAttribute(){
        if( $this->moneda == 'Euro' ){
            $res = new Moneda();
            $res->nombre = 'Euro';
            $res->abreviatura = 'Euro';
        } else {
            $res = Moneda::where('abreviatura', $this->moneda)->first();
        }
        return $res;
    }

}
