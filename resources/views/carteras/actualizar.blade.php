<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                Actualizar la cantidad de monedas en la cartera {{$cartera->nombre}}
            </h2>
            <div class="leading-loose">
                <form action="{{ route('guardarmovimiento') }}" method="post" class="p-10 bg-white rounded shadow-xl">
                    @csrf
                    @if ($errors->any())
                        <div class="bg-red-300 mb-2 border border-red-300 px-4 py-3 rounded relative">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="text-red-700">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <div class="">
                        <label class="block text-sm text-gray-600" for="descripcion">Descripción <span class="text-sm">(*)</span></label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="descripcion" 
                            name="descripcion" type="text" placeholder="Motivo de la actualización"
                            required="true" aria-label="Descripción" value="">
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="cantidad">Cantidad <span class="text-sm">(*)</span></label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cantidad" 
                            name="cantidad" type="text" placeholder="Cantidad, en la moneda de la cartera"
                            required="true" aria-label="Cantidad" value="{{$cartera->cantidad['euros']}}">
                    </div>                    
                    <div class="mt-6 text-right">
                        <input id="tipo" name="tipo" type="hidden" aria-label="Tipo" value="A">
                        <input id="fuente" name="fuente_id" type="hidden" aria-label="Fuente" value="">
                        <input id="cartera" name="cartera_id" type="hidden" aria-label="Cartera" value="{{$cartera->id}}">
                        <input id="id" name="id" type="hidden" aria-label="Id" value="">
                        <input ts="ts" name="ts" type="hidden" aria-label="Timestamp" value="{{$ts}}">
                        <button class="px-4 py-3 text-white font-light tracking-wider bg-green-500 hover:bg-green-800 rounded" type="submit">Guardar</button>
                    </div>
                    <p class="text-right text-sm">(*): Campo requerido</p>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
