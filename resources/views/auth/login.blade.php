<x-invitadocompleto-layout>
    <div class="container mx-auto h-full flex flex-1 justify-center items-center">
        <div class="w-full max-w-lg">
            <div class="leading-loose">
                <form method="POST" action="{{ route('login') }}" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
                    @csrf
                    <div class="-mt-4"><img src="/img/logo.svg" title="Gestionam-ehhh.es" class="w-1/12" /></div>
                    <p class="text-gray-800 font-medium text-center text-lg font-bold">Acceso</p>
                    <x-auth-session-status class="mb-4 font-medium text-sm text-green-600" :status="session('status')" />
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <div class="">
                        <label class="block text-sm text-gray-00" for="email">Correo electrónico</label>
                        <x-input id="email" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" type="email" name="email" :value="old('email')" placeholder="Correo electrónico" aria-label="email" required autofocus />
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="password">Contraseña</label>
                        <x-input id="password" class="w-full px-5  py-1 text-gray-700 bg-gray-200 rounded" type="password" placeholder="*******" aria-label="password" name="password" required autocomplete="current-password" />
                    </div>
                    <div class="mt-4 items-center justify-between">
                        <x-button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded">
                            Acceder
                        </x-button>
                        <label class="inline-block right-0 align-baseline  font-bold text-sm text-500 hover:text-blue-800 ml-1">
                            <input id="remember_me" type="checkbox" name="remember">
                            <span class="ml-2 text-sm text-gray-600">Recuerdamé</span>
                        </label>
                    </div>
                    <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800" href="{{ route('password.request') }}">
                        ¿Olvidó su contraseña?
                    </a>
                    &nbsp;
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">¿ Aún no se ha registrado ?</a>
                </form>
            </div>
        </div>
    </div>
</x-invitadocompleto-layout>
