<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Fuente;
use App\Models\Paginacion;

class FuentesController extends Controller {
    public function eliminar($id) {
        $fuente = Fuente::find($id);
        if(empty($fuente)) {
            return redirect( route('fuentes') )->with('error', 1);
        }
        $fuente->delete();
        return redirect( route('fuentes') )->with('exito', 1);
    }

    public function editar($id) {
        $fuente = Fuente::find($id);
        if(empty($fuente)) {
            return redirect( route('fuentes') )->with('error', 1);
        }
        return view('fuentes/form', [
            'fuente' => $fuente
        ]);
    }

    public function guardar(Request $request) {
        $datos = $request->validate([
            'nombre'=>'required|max:256',
            'url'=>'max:2000|nullable|url',
            'id'=>'nullable' 
        ]);
        if($datos["id"] != ""){
            $fuente = Fuente::find($datos["id"]);
        } else {
            $fuente = new Fuente();
        }
        $fuente->asignar($datos);
        $fuente->save();
        return redirect( route('fuentes') )->with('exito', 1);
    }

    public function index() {
        $orden = isset($_GET["orden"]) ? "balance" : "nombre";
        $fuentes = Auth::user()->fuentesConBalance()->get();
        if($orden == "balance") {
            $fuentes = $fuentes->sortByDesc($orden);
        } else {
            $fuentes = $fuentes->sortBy(function ($d, $k) use ($orden) {
                return strtolower($d[$orden]);
            });
        }
        return view('fuentes/index', [
            'orden' => $orden,
            'fuentes' => $fuentes
        ]);
    }

    public function movimientos(Request $request, $id) {
        $pagina = $request->pagina ?? 1;
        $fuente = Fuente::find($id);
        $movimientos = $fuente->movimientos()->orderBy('fecha', 'desc')
            ->limit(20)->offset(($pagina-1)*20)->get();
        $total = $fuente->movimientos()->count();
        $paginacion = new Paginacion($total, $pagina);
        return view('fuentes/movimientos', [
            'fuente' => $fuente,
            'movimientos' => $movimientos,
            'paginacion' => $paginacion
        ]);
    }

    public function nueva() {
        $fuente = new Fuente();
        return view('fuentes/form', [
            'fuente' => $fuente
        ]);
    }

}
