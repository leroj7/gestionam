<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Calendario extends Model
{
    public $mes, $ano;
    use HasFactory;

    public function anoAnterior(){
        return $this->mes > 1 ? $this->ano : $this->ano - 1;
    }

    public function anoSiguiente(){
        return $this->mes < 12 ? $this->ano : $this->ano + 1;
    }

    public function dias(){
        $dias = cal_days_in_month(CAL_GREGORIAN, $this->mes, $this->ano);
        $res = [];
        $delUsuarioEnMesActual = '.user_id='.Auth::id().' AND fecha BETWEEN "'.$this->ano.'-'.($this->mes).'-1 00:00:00" AND "'.
            $this->ano.'-'.($this->mes).'-'.cal_days_in_month(CAL_GREGORIAN, $this->mes, $this->ano).' 23:59:59"';
        $movimientos = DB::select(
            'SELECT movimientos.id, movimientos.descripcion, tipo, cantidad, moneda, cantidad_euros, fecha FROM movimientos '.
            'LEFT JOIN carteras ON movimientos.cartera_id=carteras.id WHERE movimientos'.$delUsuarioEnMesActual.' '.
            'ORDER BY fecha ASC');  
        for($i=0;$i<$dias;$i++){
            $res[$i] = [ "dia" => $i + 1, "hoy" => false ];
            $res[$i]["movimientos"] = array_filter($movimientos, function($x) use ($i){
                return strstr($x->fecha, $this->ano."-".str_pad($this->mes, 2, "0", STR_PAD_LEFT)."-".str_pad($i+1, 2, "0", STR_PAD_LEFT));
            });
        }
        return $res;
    }

    public function huecos(){
        $d = date('w', strtotime($this->ano."-".$this->mes."-1"));
        return $d == 0 ? 6 : $d - 1;
    }

    public function mesAnterior(){
        return $this->mes > 1 ? $this->mes - 1 : 12;
    }

    public function mesSiguiente(){
        return $this->mes < 12 ? $this->mes + 1 : 1;
    }

    public function nombreMes(){
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        return $meses[$this->mes-1];
    }

    public static function obtener($mes, $ano){
        $res = new Calendario();
        $res->mes = $mes;
        $res->ano = $ano;
        return $res;
    }    

}
