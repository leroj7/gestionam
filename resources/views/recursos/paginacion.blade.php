<div class="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
    <div class="flex-1 flex justify-between sm:hidden">
      <a href="{{ $paginacion->url}}&pagina={{ $paginacion->pagina > 1 ? $paginacion->pagina - 1 : 1 }}" class="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50">
        Anterior
      </a>
      <a href="{{ $paginacion->url }}&pagina={{ $paginacion->pagina < $paginacion->limiteSuperior ? $paginacion->pagina + 1 : $paginacion->limiteSuperior }}" class="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50">
        Siguiente
      </a>
    </div>
    <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
      <div>
        <p class="text-sm text-gray-700">
          Mostrando
          <!-- space -->
          <span class="font-medium">{{ $paginacion->paginaActualInf }}</span>
          <!-- space -->
          a
          <!-- space -->
          <span class="font-medium">{{ $paginacion->paginaActualSup }}</span>
          <!-- space -->
          de
          <!-- space -->
          <span class="font-medium">{{ $paginacion->total }}</span>
          <!-- space -->
          resultados
        </p>
      </div>
      <div>
        <nav class="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
          <a href="{{ $paginacion->url}}&pagina={{ $paginacion->pagina > 1 ? $paginacion->pagina - 1 : 1 }}" class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
            <span class="sr-only">Previous</span>
            <svg class="h-5 w-5" x-description="Heroicon name: solid/chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
              <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd"></path>
            </svg>
          </a>
          @if ($paginacion->pagina - 5 > 0 )
            <p class="z-10 border-gray-300 text-gray-500 relative inline-flex items-center px-4 py-2 border text-sm font-medium">
              ...
            </p>
          @endif
          @for ($i = $paginacion->limiteInferior; $i <= $paginacion->limiteSuperior; $i++)
            @if ($i >= $paginacion->pagina-5 && $i <= $paginacion->pagina + 5 )
              <a href="{{ $paginacion->url }}&pagina={{ $i }}" aria-current="page" class="z-10 {{ $i == $paginacion->pagina ? 'bg-indigo-50 border-indigo-500 text-indigo-6009' : 'border-gray-300 text-gray-500 hover:bg-gray-50' }} relative inline-flex items-center px-4 py-2 border text-sm font-medium">
                {{ $i }}
              </a>
            @endif
          @endfor
          @if ($paginacion->pagina + 5 < $paginacion->paginas )
            <p class="z-10 border-gray-300 text-gray-500 relative inline-flex items-center px-4 py-2 border text-sm font-medium">
              ...
            </p>
          @endif
          <a href="{{ $paginacion->url }}&pagina={{ $paginacion->pagina < $paginacion->limiteSuperior ? $paginacion->pagina + 1 : $paginacion->limiteSuperior }}" class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
            <span class="sr-only">Next</span>
            <svg class="h-5 w-5" x-description="Heroicon name: solid/chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
              <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
            </svg>
          </a>
        </nav>
      </div>
    </div>
</div>
