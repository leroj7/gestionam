<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarterasController;
use App\Http\Controllers\CarteraFechasController;
use App\Http\Controllers\DiasController;
use App\Http\Controllers\FuentesController;
use App\Http\Controllers\MovimientosController;
use App\Http\Controllers\TableroController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('bienvenido');
});


Route::get('/tablero', [TableroController::class, 'index'])->middleware(['auth'])->name('dashboard');

Route::get('/fuentes-de-ingresos', [FuentesController::class, 'index'])->middleware(['auth'])->name('fuentes');
Route::get('/fuentes-de-ingresos/nueva', [FuentesController::class, 'nueva'])->middleware(['auth'])->name('nuevafuente');
Route::get('/fuentes-de-ingresos/{id}/editar', [FuentesController::class, 'editar'])->middleware(['auth'])->name('editarfuente');
Route::post('/fuentes-de-ingresos/guardar', [FuentesController::class, 'guardar'])->middleware(['auth'])->name('guardarfuente');
Route::delete('/fuentes-de-ingresos/{id}', [FuentesController::class, 'eliminar'])->middleware(['auth'])->name('eliminarfuente');
Route::get('/fuentes-de-ingresos/{id}/movimientos', [FuentesController::class, 'movimientos'])->middleware(['auth'])->name('movimientosfuente');

Route::get('/carteras', [CarterasController::class, 'index'])->middleware(['auth'])->name('carteras');
Route::get('/carteras/nueva', [CarterasController::class, 'nueva'])->middleware(['auth'])->name('nuevacartera');
Route::get('/carteras/{id}/editar', [CarterasController::class, 'editar'])->middleware(['auth'])->name('editarcartera');
Route::post('/carteras/guardar', [CarterasController::class, 'guardar'])->middleware(['auth'])->name('guardarcartera');
Route::delete('/carteras/{id}', [CarterasController::class, 'eliminar'])->middleware(['auth'])->name('eliminarcartera');
Route::get('/carteras/{id}/movimientos', [CarterasController::class, 'movimientos'])->middleware(['auth'])->name('movimientoscartera');
Route::get('/carteras/{id}/actualizar', [CarterasController::class, 'actualizar'])->middleware(['auth'])->name('actualizarcantidadcartera');

Route::post('/cartera-fechas/guardar', [CarteraFechasController::class, 'guardar'])->middleware(['auth'])->name('guardarcarterafecha');

Route::get('/dia', [DiasController::class, 'index'])->middleware(['auth'])->name('dia');

Route::get('/movimientos/nuevo', [MovimientosController::class, 'nuevo'])->middleware(['auth'])->name('nuevomovimiento');
Route::get('/movimientos/{id}/editar', [MovimientosController::class, 'editar'])->middleware(['auth'])->name('editarmovimiento');
Route::post('/movimientos/guardar', [MovimientosController::class, 'guardar'])->middleware(['auth'])->name('guardarmovimiento');
Route::delete('/movimientos/{id}', [MovimientosController::class, 'eliminar'])->middleware(['auth'])->name('eliminarmovimiento');

require __DIR__.'/auth.php';
