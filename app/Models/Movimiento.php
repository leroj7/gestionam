<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class Movimiento extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function asignar($datos){
        if($datos['ts']) $this->fecha = date('Y-m-d', $datos['ts']).' '.($datos['hora'] ?? date('H:i:s'));
        $this->tipo = $datos['tipo'];
        $this->user_id = Auth::user()->id;
        $this->fuente_id = $datos['fuente_id'];
        $this->cartera_id = $datos['cartera_id'];
        if($this->tipo == 'A'){            
            $this->cantidad = $datos['cantidad'] - $this->cartera->cantidad['moneda'];
        } else {
            $this->cantidad = $datos['cantidad'];
        }
        $this->descripcion = $datos['descripcion'];
        $this->cantidad_euros = $this->cartera->objetoMoneda->enEuros($this->cantidad);
    }

    public function cartera() {
        return $this->belongsTo(Cartera::class)->withDefault();
    }

    public function getAnoAttribute(){
        $f = new \DateTime($this->fecha);
        return $f->format('Y');
    }

    public function getDiaAttribute(){
        $f = new \DateTime($this->fecha);
        return $f->format('d');
    }

    public function getFechaFormateadaAttribute(){
        $f = new \DateTime($this->fecha);
        return $f->format('d/m/Y H:i:s');
    }

    public function getHoraAttribute(){
        $f = new \DateTime($this->fecha);
        return $f->format('H:i:s');
    }

    public function getMesAttribute(){
        $f = new \DateTime($this->fecha);
        return $f->format('m');
    }

}
