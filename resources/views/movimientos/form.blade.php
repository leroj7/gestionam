<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                @if ($movimiento->id != "")
                    Cambios en el movimiento del día {{$fecha}}
                @else
                    Nueva movimiento para el día {{$fecha}}
                @endif
            </h2>
            <div class="leading-loose">
                <form action="{{ route('guardarmovimiento') }}" method="post" class="p-10 bg-white rounded shadow-xl">
                    @csrf
                    @if ($errors->any())
                        <div class="bg-red-300 mb-2 border border-red-300 px-4 py-3 rounded relative">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="text-red-700">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <div class="">
                        <label class="block text-sm text-gray-600" for="tipo">Tipo de movimiento</label>
                        <select id="tipo" name="tipo" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded">
                            <option value="I">Ingreso</option>
                            @if ($movimiento->tipo == "G")
                                <option value="G" selected="selected">Gasto</option>
                            @else
                                <option value="G">Gasto</option>
                            @endif
                            @if ($movimiento->tipo == "A")
                                <option value="A" selected="selected">Ajuste</option>
                            @else
                                <option value="A">Ajuste</option>
                            @endif
                        </select>
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="descripcion">Descripción <span class="text-sm">(*)</span></label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="descripcion" 
                            name="descripcion" type="text" placeholder="Descripción del movimiento"
                            required="true" aria-label="Descripción" value="{{$movimiento->descripcion}}">
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="fuente">Fuente</label>
                        <select id="fuente" name="fuente_id" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded">
                            @foreach ( $fuentes as $fuente)
                                @if ($movimiento->fuente_id == $fuente->id)
                                    <option value="{{$fuente->id}}">{{$fuente->nombre}}</option>
                                @else
                                    <option value="{{$fuente->id}}" selected="selected">{{$fuente->nombre}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="cartera">Cartera</label>
                        <select id="cartera" name="cartera_id" class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded">
                            @foreach ( $carteras as $cartera)
                                @if ($movimiento->cartera_id == $cartera->id)
                                    <option value="{{$cartera->id}}">{{$cartera->nombre}}</option>
                                @else
                                    <option value="{{$cartera->id}}" selected="selected">{{$cartera->nombre}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="hora">Hora <span class="text-sm">(*)</span></label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="hora" 
                            name="hora" type="time" placeholder="Hora en la que se realiza el movimiento"
                            required="true" aria-label="Hora" value="{{$movimiento->hora}}">
                    </div>                    
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="cantidad">Cantidad <span class="text-sm">(*)</span></label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cantidad" 
                            name="cantidad" type="text" placeholder="Cantidad, en la moneda de la cartera"
                            required="true" aria-label="Cantidad" value="{{$movimiento->cantidad}}">
                    </div>                    
                    <div class="mt-6 text-right">
                        <input id="id" name="id" type="hidden" aria-label="Id" value="{{$movimiento->id}}">
                        <input ts="ts" name="ts" type="hidden" aria-label="Timestamp" value="{{$ts}}">
                        <button class="px-4 py-3 text-white font-light tracking-wider bg-green-500 hover:bg-green-800 rounded" type="submit">Guardar</button>
                    </div>
                    <p class="text-right text-sm">(*): Campo requerido</p>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
