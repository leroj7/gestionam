<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Gestionam.ehhh.es</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script type="module" src="https://cdn.jsdelivr.net/npm/@ionic/core/dist/ionic/ionic.esm.js"></script>
        <script nomodule src="https://cdn.jsdelivr.net/npm/@ionic/core/dist/ionic/ionic.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ionic/core/css/ionic.bundle.css"/>        
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Scripts -->
    </head>
    <body>
        <div class="bg-gray-100 font-family-karla flex" x-data="{ confirmacion: false, actual: null }">
            <aside class="relative bg-indigo-400 h-screen w-64 hidden sm:block shadow-xl">
                <div class="p-6 text-center">
                    <a href="/tablero" class="text-white text-lg font-semibold uppercase hover:text-gray-300">
                        <img src="/img/logo.svg" title="Gestionam-ehhh.es" class="w-4/6 m-auto" />
                        <br />
                        Gestionam.ehhh.es
                    </a>
                </div>
                <nav class="text-white text-base font-semibold pt-3">
                    <a href="{{ route('dashboard') }}" class="flex items-center active-nav-link text-white py-4 pl-6 nav-item">
                        <i class="fas fa-calendar mr-3"></i>
                        Inicio
                    </a>
                    <a href="{{ route('carteras') }}" class="flex items-center active-nav-link text-white py-4 pl-6 nav-item">
                        <i class="fas fa-wallet mr-3"></i>
                        Carteras
                    </a>
                    <a href="{{ route('fuentes') }}" class="flex items-center active-nav-link text-white py-4 pl-6 nav-item">
                        <i class="fas fa-money-bill-wave"></i>
                        &nbsp;
                        Fuentes de ingresos
                    </a>
                </nav>
            </aside>
            <div class="w-full flex flex-col h-screen overflow-y-hidden">
                <header class="w-full items-center bg-white py-2 px-6 hidden sm:flex">
                    <div class="w-1/2"></div>
                    <div  x-data="{ isOpen: false }" class="relative w-1/2 flex justify-end">
                        <button @click="isOpen = !isOpen" class="realtive z-10 w-12 h-12 rounded-full overflow-hidden border-4 border-indigo-400 hover:border-indigo-300 focus:border-indigo-300 focus:outline-none">
                            <i class="fas fa-user-circle text-4xl text-indigo-400"></i>
                        </button>
                        <button x-show="isOpen" @click="isOpen = false" class="h-full w-full fixed inset-0 cursor-default"></button>
                        <div x-show="isOpen" class="absolute w-32 bg-white rounded-lg shadow-lg py-2 mt-16">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                    this.closest('form').submit();" class="block px-4 py-2 account-link hover:text-indigo-400">
                                    Desconectar
                                </x-dropdown-link>
                            </form>                            
                        </div>
                    </div>
                </header>
                <header x-data="{ isOpen: false }" class="w-full bg-indigo-400 py-5 px-6 sm:hidden">
                    <div class="flex items-center justify-between">
                        <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Admin</a>
                        <button @click="isOpen = !isOpen" class="text-white text-3xl focus:outline-none">
                            <i x-show="!isOpen" class="fas fa-bars"></i>
                            <i x-show="isOpen" class="fas fa-times"></i>
                        </button>
                    </div>
                    <nav :class="isOpen ? 'flex': 'hidden'" class="flex flex-col pt-4">
                        <a href="{{ route('dashboard') }}" class="flex items-center active-nav-link text-white py-4 pl-6 nav-item">
                            <i class="fas fa-calendar mr-3"></i>
                            Inicio
                        </a>
                        <a href="{{ route('carteras') }}" class="flex items-center active-nav-link text-white py-4 pl-6 nav-item">
                            <i class="fas fa-wallet mr-3"></i>
                            Carteras
                        </a>
                        <a href="{{ route('fuentes') }}" class="flex items-center active-nav-link text-white py-4 pl-6 nav-item">
                            <i class="fas fa-money-bill-wave"></i>
                            &nbsp;
                            Fuentes de ingresos
                        </a>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <button type="submit" class="w-full bg-white cta-btn font-semibold py-2 mt-3 rounded-lg shadow-lg hover:shadow-xl hover:bg-gray-300 flex items-center justify-center">
                                <i class="fas fa-sign-out-alt mr-3"></i> Desconectar
                            </button>
                        </form> 
                    </nav>
                </header>
                <div class="w-full overflow-x-hidden border-t flex flex-col">
                    <main class="w-full flex-grow p-6">
                        {{ $slot }}
                    </main>
                    <footer class="w-full bg-white text-right p-4">
                        Desarrollado por 
                        <a target="_blank" href="https://www.ehhh.es" class="underline" title="¡Ehhh!">
                            <img src="/img/ehhh.png" alt="Logotipo Ehhh" class="inline-block h-2/3" />
                        </a>.
                    </footer>
                </div>
            </div>
        </div>
    </body>
</html>