<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Fuente extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function asignar($datos){
        $this->nombre = $datos['nombre'];
        $this->url = $datos['url'];
        $this->user_id = Auth::user()->id;
    }

    public function movimientos() {
        return $this->hasMany(Movimiento::class);
    }

}
