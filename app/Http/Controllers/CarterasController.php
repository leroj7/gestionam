<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Cartera;
use App\Models\CarteraFecha;
use App\Models\Moneda;
use App\Models\Paginacion;

class CarterasController extends Controller {
    public function actualizar($id) {
        $cartera = Cartera::find($id);
        return view('carteras/actualizar', [
            'cartera' => $cartera,
            'ts' => time()
        ]);
    }

    public function eliminar($id) {
        $cartera = Cartera::find($id);
        if(empty($cartera)) {
            return redirect( route('carteras') )->with('error', 1);
        }
        $cartera->delete();
        return redirect( route('carteras') )->with('exito', 1);
    }

    public function editar($id) {
        $cartera = Cartera::find($id);
        if(empty($cartera)) {
            return redirect( route('carteras') )->with('error', 1);
        }
        $monedas = Moneda::select('nombre', 'abreviatura')
            ->orderBy('nombre')->get();
        return view('carteras/form', [
            'cartera' => $cartera,
            'monedas' => $monedas
        ]);
    }

    public function guardar(Request $request) {
        $datos = $request->validate([
            'nombre'=>'required|max:256',
            'descripcion'=>'max:1024|nullable',
            'moneda'=>'max:8',
            'id'=>'nullable' 
        ]);
        if($datos["id"] != ""){
            $cartera = Cartera::find($datos["id"]);
        } else {
            $cartera = new Cartera();
        }
        $cartera->asignar($datos);
        $cartera->save();
        return redirect( route('carteras') )->with('exito', 1);
    }

    public function index() {
        $orden = isset($_GET["orden"]) ? "balance" : "nombre";
        $carteras = Auth::user()->carteras()->get();
        foreach($carteras as $c) {
            $balance = $c->cantidad;
            $c->cantidad_moneda = $balance['moneda'];
            $c->cantidad_euros = $balance['euros'];
        }
        if($orden == "balance") {
            $carteras = $carteras->sortByDesc('cantidad_euros');
        } else {
            $carteras = $carteras->sortBy(function ($d, $k) use ($orden) {
                return strtolower($d[$orden]);
            });
        }
        return view('carteras/index', [
            'orden' => $orden,
            'carteras' => $carteras
        ]);
    }

    public function movimientos(Request $request, $id) {
        $pagina = $request->pagina ?? 1;
        $cartera = Cartera::find($id);
        $movimientos = $cartera->movimientos()->orderBy('fecha', 'desc')
            ->limit(20)->offset(($pagina-1)*20)->get();
        $total = $cartera->movimientos()->count();
        $paginacion = new Paginacion($total, $pagina);
        return view('carteras/movimientos', [
            'cartera' => $cartera,
            'movimientos' => $movimientos,
            'paginacion' => $paginacion
        ]);
    }

    public function nueva() {
        $cartera = new Cartera();
        $monedas = Moneda::select('nombre', 'abreviatura')
            ->orderBy('nombre')->get();
        return view('carteras/form', [
            'cartera' => $cartera,
            'monedas' => $monedas
        ]);
    }

}
