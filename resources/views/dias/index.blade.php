<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                Movimientos correspondientes al día {{$dia}}/{{$mes}}/{{$ano}}
            </h2>
            <p class="text-right">
                <a href="{{ route('nuevomovimiento', ['ts' => $ts]) }}" title="Nueva fuente">
                    <button class="bg-green-500 hover:bg-green-800 text-white font-bold py-3 px-4 rounded">
                        Nuevo movimiento
                    </button>
                </a>
            </p>
            <div class="bg-white rounded-lg shadow-xl overflow-auto mt-2">
                <table class="min-w-full bg-white">
                    <thead class="bg-indigo-400 text-white">
                        <tr>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Hora</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Movimiento</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Cantidad</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">&euro;</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm"></th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-700">
                        @foreach ($movimientos as $movimiento)
                            <tr >
                                <td class="text-left py-3 px-4">{{ $movimiento->hora }}</td>
                                <td class="text-left py-3 px-4">{{ $movimiento->descripcion }}</td>
                                <td class="text-left py-3 px-4 {{ $movimiento->tipo=='I' ? 'text-green-400' : ($movimiento->tipo=='A' ? 'text-indigo-400' : 'text-red-400')}}">{{ $movimiento->cantidad }} {{ $movimiento->cartera->moneda}}</td>
                                <td class="text-left py-3 px-4 {{ $movimiento->tipo=='I' ? 'text-green-400' : ($movimiento->tipo=='A' ? 'text-indigo-400' : 'text-red-400')}}">{{ round($movimiento->cantidad_euros, 2) }} &euro;</td>
                                <td class="text-left py-3 px-4">
                                    <a href="{{ route( 'editarmovimiento', [$movimiento->id]) }}" class="bg-green-500 hover:bg-green-800 cursor-pointer rounded p-1 mx-1 text-white tab-button">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    @include('recursos.botonBorrar', ['objeto' => $movimiento, 'ruta' => route('eliminarmovimiento', [$movimiento->id])])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('recursos.confirmarEliminacion', [])
</x-app-layout>
