<form action="{{ $ruta }}" id="d{{$objeto->id}}" method="post" class="inline">
    @csrf
    {{ method_field('DELETE')}}
    <a @click="confirmacion=true, actual={{$objeto->id}}" class="bg-red-500 hover:bg-red-800 cursor-pointer rounded p-1 mx-1 text-white tab-button">
        <i class="fas fa-trash"></i>
    </a>
</form>
