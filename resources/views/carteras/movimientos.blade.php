<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mb-3">
                Movimientos para la cartera "{{$cartera->nombre}}"
            </h2>
            <div class="bg-white rounded-lg shadow-xl overflow-auto mt-2">
                <table class="min-w-full bg-white">
                    <thead class="bg-indigo-400 text-white">
                        <tr>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Fecha</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Movimiento</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Cantidad</th>
                            <th class="text-left py-3 px-4 uppercase font-semibold text-sm">&euro;</th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-700">
                        @foreach ($movimientos as $movimiento)
                            <tr >
                                <td class="text-left py-3 px-4">{{ $movimiento->fecha_Formateada }}</td>
                                <td class="text-left py-3 px-4">{{ $movimiento->descripcion }}</td>
                                <td class="text-left py-3 px-4 {{ $movimiento->tipo=='I' ? 'text-green-400' : ($movimiento->tipo=='A' ? 'text-indigo-400' : 'text-red-400')}}">{{ $movimiento->cantidad }} {{ $movimiento->cartera->moneda }}</td>
                                <td class="text-left py-3 px-4 {{ $movimiento->tipo=='I' ? 'text-green-400' : ($movimiento->tipo=='A' ? 'text-indigo-400' : 'text-red-400')}}">{{ round($movimiento->cantidad_euros, 2) }} &euro;</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @include('recursos/paginacion', [ 'paginacion' => $paginacion ])
            </div>
        </div>
    </div>
</x-app-layout>
